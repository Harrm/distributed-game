extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const GRAVITY = 980
var velocity = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 100 * delta
	elif Input.is_action_pressed("ui_right"):
		velocity.x += 100 * delta
	else:
		velocity.x = 0
	if abs(velocity.x) > 100:
		velocity.x /= abs(velocity.x) / 100
	velocity.y += GRAVITY * delta
	velocity = move_and_slide_with_snap(velocity, Vector2.DOWN * 10, Vector2.UP)
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass


func _input(_event):
	pass
