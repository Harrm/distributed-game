# Distributed Game

## Abstract
Tournaments where mages control **fantasy creatures** to win the prize. Battles are performed in **duels**, the only winner receives the prize. The game is fully **decentralized** and is based on a **peer-to-peer** architecture.

## The Game Flow
### Phases
#### Registration
Players should **sign up** for the tournament before it starts. A player can sign up for any tournament with free places.

A player not involved in any tournament can **announce** a tournament, stating the max number of players in it (from 3 to 32). In this case, other players will be able to connect to it.

Once the limit of players required for the tournament is reached, the players are **randomly divided in pairs**. Every pair conducts a duel, and the **winners** are randomly divided in pairs again, and this procedure **repeats** until only **one pair** of players is left.  

If the number of players is **odd**, then one random player automatically proceeds to the next level. _This player cannot be chosen again if this situation reoccurs in the next level of competition_.

#### Duel
Duels are turn-based.

Every player has 5 **lives**. Once a player's amount of lives reaches 0, the player is **defeated**.
A player's **avatar** resides in a **fixed area** on a map and _cannot_ move. 

Every creature has 3 attributes: **health** and **strength**. Once a creature's health reaches 0, it disappears and is no longer available. It's strength indicates how much **damage** it inflicts when hits another creature. The amount of lives reduced from a **player** hit by a creature attack is **always** 0.

### Tournament conclusion
Once the last pair's duel is finished, the winner becomes the winner of the tournament, and every player becomes available for a new competition.

## Implementation
![network](docs/img/network.png)
Peers are connected with a several overlay networks, called _meshes_ in gossipsub. One of them, the global network, unites all peers desiring to participate in the game. It is used to announce tournaments, sign up for them and so on. Other meshes are used for communications of players on the same tournament exclusively. They are indicated with colored connections on the picture.


The implementation is separated on several layers, where lower layers are completely independent of upper layers (Clean Architecture approach).

### Middleware
The lowest layer, directly communicates with LibP2P. It provides simple messaging abstraction for LibP2P transport, allowing to register handlers for messages and to broadcast messages on a network. Its API is represented by GossipService trait which can be found in [native/src/middleware/network.rs](native/src/middleware/network.rs).
It basically provides two methods:
```rust
    pub fn register_handler(&mut self, msg_type: MsgType, cb: Handler);
    pub fn gossip_message(&mut self, msg: Msg);
```
Which are enough for a node's communication with the outside world. A node maintain a separate _GossipService_ for every mesh it requires, but Gossipsub message topics could be also used for that (details in the [Gossipsub Spec](https://github.com/libp2p/specs/blob/master/pubsub/gossipsub/gossipsub-v1.0.md)). 

### Matchmaking
The logic of creating, filling up and ending tournaments, also division of players in rooms in one tournament. _Not currently implemented_.
#### Messages
Definitions of messages in this layer can be found in [native/src/matchmaking/messages.rs](native/src/matchmaking/messages.rs).
* ANNOUNCE_TOURNAMENT (limit: u8, name: string, id: u32)
* CANCEL_TOURNAMENT (id: u32)
* SIGN_UP (tournament_id: u32, player_id: u32)
* ACCEPT (tournament_id: u32, player_id: u32)
* REJECT (tournament_id: u32, player_id: u32)
* START_TOURNAMENT (id: u32)
* END_TOURNAMENT (id: u32, victor: u32)

### Gameplay runtime
Gameplay is implemented as a state-transition function, defined in [native/src/gameplay/mod.rs](native/src/gameplay/mod.rs). It maintains a state for every room and changes it by processing a fixed set of actions. It makes implementation of game nodes much easier, for they require just to maintain a set of game rooms and pass all the messages concerning the game flow to them, deserializing them from a network format.

#### Actions
The list of actions follows the simple game rules, where players first pick their creatures from a fixed set, and then control them to eliminate the opponent's creatures and with this win.
* PICK (creature_name: string) // pick a creature during creature selection phase
* MOVE (creature_name: string, target_pos: position) // command a creature to move
* ATTACK (creature_name: string, target_name: string) // command a creature to attack a hostile creature
* SURRENDER // self-descriptive

A simple example how this runtime works can be found in the tests module in the gameplay source file ([this one](native/src/gameplay/mod.rs)).

![Gameplay flow](docs/img/duel_flow.png)

### Rust
Rust is a C-like system programming language, dedicated to safety and performance. With its borrow checking mechanism it allows to write code which memory correctness is checked at _compile-time_. This proves especially useful in asynchronous programming on a shared memory.

[Official site](https://www.rust-lang.org/)

### Libp2p
LibP2P is a modular networking library aimed at flexibility and simplicity. It is a part of IPFS networking stack, so it is mostly built around peer-to-peer technologies. 

[Official site](https://libp2p.io/)

### Gossipsub
A general purpose publisher-subscriber protocol based on gossiping and topic meshes. It differs from common gossip protocols in message routing: instead of flooding the network by sending messages to all known peers, a node following gossipsub protocol would only send messages to a fixed subset of its peers. Such connections form a _topic mesh_, a network overlay with bidirectional channels uniting peers interested in one "topic". Parameters of forming such meshes are fully customizible to offer optimal trade-off between network congestion and latency.

[Specification](https://github.com/libp2p/specs/blob/master/pubsub/gossipsub/gossipsub-v1.0.md) 

## Gossipsub Example
To run an example with gossipsub protocol in action, change your current dir to native/ and run in one terminal window:
```shell script
cargo run --bin gossipsub_chat
```
, then peek an address of the started node in the output (typically /ip4/127.0.0.1/tcp/...), and then this run in another window:
```shell script
cargo run --bin gossipsub_chat -- ADDRESS
```
You will then be able to exchange messages between these two instances by simply typing text in an either of the terminal windows.
The communications in the game itself occurs in a similar manner, with the only difference that the messages are serialized data structures containing info about the game flow.
![example](docs/img/gossipsub_example.png)

## Conclusions
Although the project is not yet complete enough to demonstrate a fully working demo, the main parts of it are done: the state-transition runtime, which defines the rules of the game, and the gossiping service, which allows the players to communicate. What is left to be done is:
* Connect the gameplay runtime to the network messages via GossipServer handlers
* Implement tournament registration and duel partition logic
* Implement peer discovery mechanism (mostly covered by Libp2p Gossipsub)
* Address security concerns via proper consensus and authentication mechanisms
* Set up a Docker configuration for the demo network

(Where only the first and partly the second part are a part of the course project requirements, ahem).