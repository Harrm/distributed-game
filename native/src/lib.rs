#![feature(box_syntax)]
#![deny(rust_2018_idioms)]
#![deny(future_incompatible)]
#![warn(nonstandard_style)]
#![allow(missing_docs)]
#![warn(unreachable_pub)]
#![allow(unused)]

//! Root of the native code

/// low-level network stuff
pub mod middleware;

/// tournament logic
pub mod matchmaking;

/// gameplay logic engine
pub mod gameplay;

/// Bindings from Rust native code to Godot
pub mod node;
