use libp2p::gossipsub::{GossipsubMessage, MessageAuthenticity, Topic};
use std::collections::hash_map::DefaultHasher;
use libp2p::gossipsub;
use std::time::Duration;
use std::hash::{Hash, Hasher};
use libp2p::gossipsub::protocol::MessageId;
use env_logger::{Builder, Env};
use libp2p::core::identity;
use libp2p::PeerId;
use std::error::Error;


fn main() -> Result<(), Box<dyn Error>> {
    Builder::from_env(Env::default().default_filter_or("info")).init();

    // Create a random PeerId
    let local_key = identity::Keypair::generate_ed25519();
    let local_peer_id = PeerId::from(local_key.public());

    // Set up an encrypted TCP Transport over the Mplex and Yamux protocols
    let transport = libp2p::build_development_transport(local_key.clone())?;

    // Create a Gossipsub topic
    let topic = Topic::new("tournament".into());

    // Create a Swarm to manage peers and events
    let _swarm = {
        // to set default parameters for gossipsub use:
        // let gossipsub_config = gossipsub::GossipsubConfig::default();

        // To content-address message, we can take the hash of message and use it as an ID.
        let message_id_fn = |message: &GossipsubMessage| {
            let mut s = DefaultHasher::new();
            message.data.hash(&mut s);
            MessageId::from(s.finish().to_string())
        };

        // set custom gossipsub
        let gossipsub_config = gossipsub::GossipsubConfigBuilder::new()
            .heartbeat_interval(Duration::from_secs(10))
            .message_id_fn(message_id_fn) // content-address messages. No two messages of the
            //same content will be propagated.
            .build();
        // build a gossipsub network behaviour
        let mut gossipsub =
            gossipsub::Gossipsub::new(MessageAuthenticity::Signed(local_key), gossipsub_config);
        gossipsub.subscribe(topic.clone());
        libp2p::Swarm::new(transport, gossipsub, local_peer_id)
    };

    // let mut service: GossipService<MessageType, Message, Box<dyn Fn(&Message)>> = GossipService::create(swarm);
    // service.start();
    Ok(())
}