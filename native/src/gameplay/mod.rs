use std::error::Error;
use std::fmt::{Display, Result as FmtResult, Formatter};
use serde::{Deserialize, Serialize};

struct PlayerAvatar {
    health: u8,
}

impl Default for PlayerAvatar {
    fn default() -> Self {
        PlayerAvatar {health: 5}
    }
}

#[derive(Clone)]
pub struct CreatureStats {
    health: u8,
    strength: u8,
    name: String,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Position {
    x: u8,
    y: u8
}

impl Position {
    fn distance(&self, p: &Position) -> u8 {
        ((self.x as i16 - p.x as i16).abs() + (self.y as i16 - p.y as i16).abs()) as u8
    }

    fn xy(x: u8, y: u8) -> Position {
        Position {x, y}
    }
}

#[derive(Clone)]
struct Creature {
    lives: u8,
    pos: Position,
    stats: CreatureStats
}

struct Player {
    creatures: Vec<Creature>,
    avatar: PlayerAvatar,
    name: String,
}

impl Player {

    fn get_creature_idx(&self, creature_name: &str) -> Option<usize> {
        self.creatures.iter().enumerate().find(|c| c.1.stats.name == creature_name)
            .map(|o| o.0)
    }
}

pub enum GameState {
    Pick {
        creatures_pool: Vec<CreatureStats>,
    },
    Battle {
        turn_list: Vec<String>,
        whose_turn: String,
    },
    Over {
        victor_name: String
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub enum Action {
    Pick {
        creature_name: String,
    },
    Move {
        creature_name: String,
        target_pos: Position
    },
    AttackCreature {
        creature_name: String,
        target_name: String
    },
    AttackPlayer {
        creature_name: String,
        target_name: String
    },
    Surrender
}


/// Error in a gameplay flow
#[derive(Debug)]
pub enum GameplayError {
    /// Player with the provided name is not found
    PlayerNotFound,
    /// All player's spawn points are busy
    NoSpawnAvailable,
    /// attempt to command a hostile creature
    ControlHostileCreature,
    /// creature with this name is not found
    CreatureNotFound,
    /// target is too far away
    TargetOutOfReach,
    /// refer to a cell outside of the battlefield
    OutOfBattlefield,
    /// try to move to an occupied cell
    CellIsOccupied,
    /// creature position doesn't match what actually is at the field's cell
    InvalidCreaturePosition,
    /// attempt to pick a creature when Pick state is already over
    PickStateOver,
    /// attempt to perform a battle action when the battle is over
    BattleStateOver,
}

impl Display for GameplayError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{:?}", self)
    }
}

impl Error for GameplayError {

}

pub trait GameplayCore<'a> {
    fn new_game(player_names: Vec<String>, creatures_pool: Vec<CreatureStats>) -> Self;

    fn process_action(&mut self, actor_player: &str, action: Action) -> Result<(), GameplayError>;

    fn is_over(&self) -> bool;

    fn get_state(&self) -> &GameState;
}

#[derive(Clone)]
enum FieldCell<'a> {
    Empty,
    Creature(String),
    Player(&'a PlayerAvatar),
}

struct BattleField<'a> {
    cells: Vec<FieldCell<'a>>,
    width: u8,
    height: u8,
}

impl<'a> BattleField<'a> {
    fn create(width: u8, height: u8) -> BattleField<'a> {
        BattleField {cells: vec![FieldCell::Empty; (width * height) as usize], width, height}
    }


    fn get_cell_mut(&mut self, pos: Position) -> Option<&mut FieldCell<'a>> {
        if pos.x < self.width && pos.y < self.height {
            return None;
        }
        let idx = pos.x + pos.y * self.height;
        Some(&mut self.cells[idx as usize])
    }

    fn get_cell(&self, pos: &Position) -> Option<&FieldCell<'a>> {
        debug_assert!(pos.x < self.width && pos.y < self.height);
        if pos.x >= self.width || pos.y >= self.height {
            return None;
        }
        let idx = pos.x + pos.y * self.height;
        Some(&self.cells[idx as usize])
    }

    fn set_cell(&mut self, pos: &Position, cell: FieldCell<'a>) {
        debug_assert!(pos.x < self.width && pos.y < self.height);
        let idx = pos.x + pos.y * self.height;
        self.cells[idx as usize] = cell;
    }

    fn swap_cells_unchecked(&mut self, pos1: &Position, pos2: &Position) {
        let idx1 = (pos1.x + pos1.y * self.height) as usize;
        let idx2 = (pos2.x + pos2.y * self.height) as usize;
        self.cells.swap(idx1, idx2);
    }
}

pub struct GameSession<'a> {
    players: Vec<Player>,
    state: GameState,
    field: BattleField<'a>
}

impl<'a> GameSession<'a> {
    /// returns owner's idx and creature's idx
    fn find_creature(&self, creature_name: &str) -> Option<(usize, usize)> {
        for p in self.players.iter().enumerate() {
            if let Some(idx) = p.1.get_creature_idx(creature_name) {
                return Some((p.0, idx));
            }
        }
        None
    }

    fn find_player(&self, player_name: &str) -> Option<&Player> {
        self.players.iter()
            .find(|p| p.name == player_name)
    }

    fn find_player_mut(&mut self, player_name: &str) -> Option<&mut Player> {
        self.players.iter_mut()
            .find(|p| p.name == player_name)
    }

    fn get_available_spawn(&self, player_name: &str) -> Option<Position> {
        //!   0 1 2 3 4
        //!  +=========+
        //! 0|- * 1 * -|
        //! 1|- - * - -|
        //! 2|- - - - -|
        //! 3|- - * - -|
        //! 4|- * 2 * -|
        //! |=========|
        assert_eq!(self.players.len(), 2, "Only two players are allowed in this version");
        let first_player_spawns = vec![Position::xy(1, 0), Position::xy(3, 0), Position::xy(2, 1)];
        let second_player_spawns = vec![Position::xy(1, 4), Position::xy(3, 4), Position::xy(2, 3)];
        let this_player_spawns = if player_name == self.players.first().unwrap().name {
            &first_player_spawns
        } else {
            &second_player_spawns
        };
        for spawn in this_player_spawns {
            match self.field.get_cell(spawn).unwrap() {
                FieldCell::Empty => { return Some(spawn.clone()); }
                FieldCell::Creature(_) => {}
                FieldCell::Player(_) => {}
            }
        }
        None
    }
}

impl<'a> GameplayCore<'a> for GameSession<'a> {
    fn new_game(player_names: Vec<String>, creatures_pool: Vec<CreatureStats>) -> GameSession<'a> {
        GameSession {
            players: player_names.iter().map(
                |name| Player { creatures: Vec::new(), avatar: Default::default(), name: name.to_owned() }).collect(),
            state: GameState::Pick { creatures_pool },
            field: BattleField::create(5, 5)
        }
    }

    fn process_action(&mut self, player_name: &str, action: Action) -> Result<(), GameplayError> {
        match action {
            Action::Pick {creature_name} => {
                let mut pick_ended = false;
                let stats = match &mut self.state {
                    GameState::Pick {ref mut creatures_pool} => {
                        let (idx, stats) = creatures_pool.iter().enumerate()
                            .find(|(_, c)| c.name == creature_name)
                            .map(|(i, c)| (i, c.clone()))
                            .ok_or(GameplayError::CreatureNotFound)?;
                        creatures_pool.remove(idx);
                        if creatures_pool.is_empty() {
                            pick_ended = true;
                        }
                        stats
                    },
                    _ => return Err(GameplayError::PickStateOver)
                };
                let pos = self.get_available_spawn(player_name).ok_or(GameplayError::NoSpawnAvailable)?;
                let creature_name = {
                    let creatures = &mut self.find_player_mut(player_name)
                        .ok_or(GameplayError::PlayerNotFound)?.creatures;
                    creatures.push(Creature {lives: stats.health, stats: stats.clone(), pos});
                    stats.name
                };
                self.field.set_cell(&pos, FieldCell::Creature(creature_name));

                if pick_ended {
                    self.state = GameState::Battle {
                        turn_list: self.players.iter().map(|p| p.name.clone()).collect(),
                        whose_turn: self.players.first().unwrap().name.clone()};
                }
            },
            Action::Move {creature_name, target_pos  } => {
                match self.state {
                    GameState::Battle { .. } => {},
                    _ => { return Err(GameplayError::BattleStateOver) }
                }
                let target_cell =
                    self.field.get_cell(&target_pos).ok_or(GameplayError::OutOfBattlefield)?;
                match target_cell {
                    FieldCell::Creature {..} | FieldCell::Player(..) => return Err(GameplayError::CellIsOccupied),
                    FieldCell::Empty => ()
                };
                let player = self.find_player(player_name).ok_or(GameplayError::PlayerNotFound)?;
                let creature = player.creatures.iter()
                    .find(|c| c.stats.name == creature_name)
                    .ok_or(GameplayError::ControlHostileCreature)?;
                let starting_cell =
                    self.field.get_cell(&creature.pos).ok_or(GameplayError::OutOfBattlefield)?;
                match starting_cell {
                    FieldCell::Creature(c) => {
                        if *c != creature.stats.name { return Err(GameplayError::InvalidCreaturePosition) }
                    },
                    _ => return Err(GameplayError::InvalidCreaturePosition)
                };
                let starting_pos = creature.pos;
                self.field.swap_cells_unchecked(&target_pos, &starting_pos);

                let player = self.find_player_mut(player_name).ok_or(GameplayError::PlayerNotFound)?;
                let creature = player.creatures.iter_mut()
                    .find(|c| c.stats.name == creature_name)
                    .ok_or(GameplayError::ControlHostileCreature)?;
                creature.pos = target_pos;

            }
            Action::AttackCreature {creature_name, target_name} => {
                match self.state {
                    GameState::Battle { .. } => {},
                    _ => { return Err(GameplayError::BattleStateOver) }
                }
                let creature = {
                    let acting_player = self.find_player(player_name).ok_or(GameplayError::PlayerNotFound)?;
                    acting_player.creatures.iter()
                    .find(|c| c.stats.name == creature_name)
                    .ok_or(GameplayError::ControlHostileCreature)?.clone()
                };

                let (owner_idx, target_idx) = self.find_creature(&target_name)
                    .ok_or(GameplayError::CreatureNotFound)?;
                let target_owner = &mut self.players[owner_idx];
                let target_mut = &mut target_owner.creatures[target_idx];

                if creature.pos.distance(&target_mut.pos) > 1 {
                    return Err(GameplayError::TargetOutOfReach);
                }

                if target_mut.lives > creature.stats.strength {
                    target_mut.lives -= creature.stats.strength;
                } else {
                    target_mut.lives = 0;
                    target_owner.creatures.remove(target_idx);
                }

                if target_owner.creatures.len() == 0 {
                    self.state = GameState::Over { victor_name: player_name.to_owned() };
                }
            },
            Action::AttackPlayer {..} => {

            },
            Action::Surrender => {
                let idx = self.players.iter()
                    .enumerate().find(|p| p.1.name == player_name)
                    .ok_or(GameplayError::PlayerNotFound)?.0;
                self.players.remove(idx);
                if self.players.len() == 1 {
                    self.state = GameState::Over {victor_name: (&self.players[0].name).to_owned()};
                };
            }
        }
        Ok(())
    }

    fn is_over(&self) -> bool {
        match self.state {
            GameState::Over {..} => true,
            _ => false
        }
    }

    fn get_state(&self) -> &GameState {
        &self.state
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::error::Error;

    #[test]
    fn dead_creature_dies() -> Result<(), Box<dyn Error>>{
        let mut game = GameSession::new_game(
            vec!["Alice".to_owned(), "Bob".to_owned()],
            vec![CreatureStats{name: "A".to_owned(), health: 3, strength: 2},
                             CreatureStats{name: "B".to_owned(), health: 2, strength: 3}]);
        assert!(!game.is_over());
        game.process_action("Alice", Action::Pick {creature_name: "A".to_owned()})?;
        game.process_action("Bob", Action::Pick {creature_name: "B".to_owned()})?;
        game.process_action("Alice",
                            Action::Move {creature_name: "A".to_owned(), target_pos: Position::xy(1, 3)})?;
        game.process_action("Alice",
                            Action::AttackCreature {creature_name: "A".to_owned(), target_name: "B".to_owned()})?;
        assert!(game.is_over());
        Ok(())
    }
}