
pub type TournamentSize = u8;
pub type TournamentId = u64;
pub type RoomId = u64;
pub type PlayerId = u64;

pub mod messages;
pub mod channels;
mod tournament_service;
