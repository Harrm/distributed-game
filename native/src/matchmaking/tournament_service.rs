use crate::middleware::network::{GossipService, Message as NetworkMessage};
use super::messages::{MessageType, Message};
use super::{TournamentId, PlayerId, RoomId};
use serde::{Serialize, Deserialize};
use crate::gameplay::{Action, GameplayCore, GameSession};
use std::collections::HashMap;

enum MessageTypeName {
    Move = 0,
    Attack,
    Pick,
    Surrender,
}

#[derive(Serialize, Deserialize)]
struct TournamentMessage {
    id: TournamentId,
    room_id: RoomId,
    invoker: PlayerId,
    action: Action,
}

impl NetworkMessage<MessageType> for TournamentMessage {
    fn get_type(&self) -> MessageType {
        match self.action {
            Action::Pick { .. } => { MessageTypeName::Pick as MessageType }
            Action::Move { .. } => { MessageTypeName::Move as MessageType }
            Action::AttackCreature { .. } => { MessageTypeName::Attack as MessageType }
            Action::AttackPlayer { .. } => { MessageTypeName::Attack as MessageType }
            Action::Surrender => { MessageTypeName::Surrender as MessageType }
        }
    }
}

type TournamentGossipService = GossipService<MessageType, TournamentMessage, Box<dyn Fn(&TournamentMessage)>>;

struct Room<'a> {
    game: GameSession<'a>,
    players: HashMap<PlayerId, String>,
}

struct TournamentService<'a> {
    gossip_service: TournamentGossipService,
    rooms: Vec<Room<'a>>,
}

impl<'a> TournamentService<'a> {

    fn create(mut gossip: TournamentGossipService) -> TournamentService<'a> {
        let mut this = TournamentService { gossip_service: gossip, rooms: Default::default() };
        // let handler = box |msg| this.process_action(msg);
        //  this.gossip_service.register_handler(MessageTypeName::Pick as MessageType,
        //                                       handler);
        this
    }

    fn process_action(&mut self, msg: &TournamentMessage) {
        let room = &mut self.rooms[msg.room_id as usize];
        let res = room.game.process_action(&room.players[&msg.invoker],
                                           msg.action.clone());
        if let Err(e) = res {
            println!("{:?}", e);
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn simple_gameplay_scenario() {

    }

}