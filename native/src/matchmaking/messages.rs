use crate::matchmaking::{PlayerId, TournamentId, TournamentSize};
use crate::middleware::network::Message as NetworkMessage;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub enum RejectReason {
    TournamentFull,
}

#[derive(Serialize, Deserialize)]
pub enum TournamentOutcome {
    Victory {
        victor: PlayerId
    },
}

pub type MessageType = u8;

#[derive(Serialize, Deserialize)]
pub enum Message {
    AnnounceTournament {
        id: TournamentId,
        name: String,
        size: TournamentSize,
    },
    CancelTournament {
        id: TournamentId,
    },
    SignUp {
        tournament_id: TournamentId,
        player_id: PlayerId,
    },
    Accept {
        tournament_id: TournamentId,
        player_id: PlayerId,
    },
    Reject {
        tournament_id: TournamentId,
        player_id: PlayerId,
        reason: RejectReason,
    },
    StartTournament {
        id: TournamentId,
    },
    EndTournament {
        id: TournamentId,
        outcome: TournamentOutcome,
    }
}

impl NetworkMessage<MessageType> for Message {
    fn get_type(&self) -> MessageType {
        match self {
            Message::AnnounceTournament { .. } => 0,
            Message::CancelTournament { .. } => 1,
            Message::SignUp { .. } => 2,
            Message::Accept { .. } => 3,
            Message::Reject { .. } => 4,
            Message::StartTournament { .. } => 5,
            Message::EndTournament { .. } => 6,
        }
    }
}