use super::{TournamentSize, TournamentId};

trait TournamentChannel {
    fn get_participants(&self) -> Vec<String>;
    fn count_participants(&self) -> TournamentSize;
    fn has_started(&self) -> bool;
    fn get_id(&self) -> TournamentId;
}

trait TournamentOwningChannel: TournamentChannel {
    fn start_tournament(&mut self);
    fn cancel_tournament(&mut self);
}

trait GlobalChannel {
    fn announce_tournament(&mut self, participants_limit: TournamentSize) -> dyn TournamentOwningChannel;
    fn find_tournament(&self) -> dyn TournamentChannel;
}
