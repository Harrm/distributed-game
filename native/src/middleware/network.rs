use std::collections::HashMap;
use std::task::{Poll, Context};
use std::marker::PhantomData;
use futures::prelude::*;
use async_std::{task};
use libp2p::gossipsub::{GossipsubEvent, GossipsubMessage, Gossipsub, Topic};
use serde::{Serialize};
use libp2p::{Swarm};
use serde::de::DeserializeOwned;
use std::hash::Hash;
use std::error::Error;

/// A gossip message
pub trait Message<Type>: Serialize + DeserializeOwned {
    fn get_type(&self) -> Type;
}

///
/// A service that supplies gossip protocol capabilities
///
pub struct GossipService<
    MsgType,
    Msg: Message<MsgType>,
    Handler: Fn(&Msg)> {
    swarm: Swarm<Gossipsub>,
    topic: Topic,
    callbacks: HashMap<MsgType, Handler>,
    phantom: PhantomData<Msg>,
}

impl<'call,
    MsgType: Eq + Hash,
    Msg: Message<MsgType>,
    Handler: Fn(&Msg) + 'call>
GossipService<MsgType, Msg, Handler> {

    /// creates an instance of GossipService with the given peer swarm
    pub fn create(swarm: Swarm<Gossipsub>) -> GossipService<MsgType, Msg, Handler> {
        GossipService { swarm, topic: Topic::new("tournament".into()), callbacks: HashMap::new(), phantom: PhantomData }
    }

    /// run the service asynchronously
    pub fn start(&mut self) {
        libp2p::Swarm::listen_on(&mut self.swarm, "/ip4/0.0.0.0/tcp/0".parse().unwrap()).unwrap();
        let mut listening = false;
        task::block_on(future::poll_fn(move |ctx: &mut Context<'_>| {
            if let Err(e) = self.receive_messages(ctx) {
                println!("Error receiving messages: {:?}", e);
            }

            if !listening {
                for addr in libp2p::Swarm::listeners(&self.swarm) {
                    println!("Listening on {:?}", addr);
                    listening = true;
                }
            }

            Poll::Pending
        }))
    }

    /// register the handler for a message type
    pub fn register_handler(&mut self, msg_type: MsgType, cb: Handler) {
        self.callbacks.insert(msg_type, cb);
    }

    /// gossip a message
    pub fn gossip_message(&mut self, msg: Msg) -> Result<(), Box<dyn Error>> {
        let enc_msg = serde_cbor::to_vec(&msg)?;
        self.swarm.publish(&self.topic, enc_msg).unwrap();
        Ok(())
    }

    fn receive_messages(&mut self, ctx: &mut Context<'_>) -> Result<(), Box<dyn Error>> {
        loop {
            match self.swarm.poll_next_unpin(ctx) {
                Poll::Ready(Some(gossip_event)) => match gossip_event {
                    GossipsubEvent::Message(_peer_id, _id, message)
                    => self.process_message(message)?,
                    _ => {}
                },
                Poll::Ready(None) | Poll::Pending => break,
            }
        }
        Ok(())
    }

    fn process_message(&mut self, message: GossipsubMessage) -> Result<(), Box<dyn Error>> {
        let msg: Msg = serde_cbor::from_slice(&message.data)?;
        if let Some(handler) = self.callbacks.get(&msg.get_type()) {
            handler(&msg);
        }
        Ok(())
    }
}
